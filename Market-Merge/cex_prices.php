<?php
/*  For all atomic-dex community-coin-buy-markets the currency-price of the related asset is looked-up on coinpaprika and coingecko
    The Tikker-symbol can not allways be used; For coingecho, where necessary, a coin-id translation-table is used ($genuine)
    Coingecko public API requires attribution; Calls are limited to 1/hour ~ 720/month
    
    https://www.bandprotocol.com/standard-dataset
*/

define("ROOT","./data/");
$genuine=explode(",","bitcoin-cash,binancecoin,digitalcoin,dogecoin,ethereum,zcoin,flux,feathercoin,groestlcoin,litecoin,monacoin,terracoin,unobtanium,vcash");
$genuine=array_flip($genuine);
$password=file_get_contents(ROOT."password");

$url="https://api.coinpaprika.com/v1/ticker?coin_id=BTC&quotes=BTC,USD,EUR";
$paprika_result=file_get_contents($url);
if (strpos($paprika_result,'"symbol":"BTC"')>0){
	file_put_contents(ROOT."paprika.txt",$paprika_result);
}else{
	file_put_contents(ROOT."paprika.log",$paprika_result);
	$paprika_result=file_get_contents("paprika.txt");
}
$url="https://api.coingecko.com/api/v3/coins/list";
$gecko_result=file_get_contents($url);
if (strpos($gecko_result,'"id":"bitcoin"')>0){
	file_put_contents(ROOT."gecko.sym",$gecko_result);
} else {
	file_put_contents(ROOT."gecko.log",$gecko_result);
	$gecko_result=file_get_contents(ROOT."gecko.sym");
}

$paprika=json_decode(file_get_contents(ROOT."paprika.txt"),1);
$gecko=json_decode(file_get_contents(ROOT."gecko.sym"),1);

$cccoins=explode(",",file_get_contents(ROOT."cccoins"));
$result_all="";
foreach ($cccoins as $one) {
	$result[$one]=pairs($one,$password);
	$result_all.=$result[$one];
}
$all=explode("\n",$result_all);
sort($all);

foreach ($all as $one) {
	$unique[$one]['price_btc']=0;
	$unique[$one]['gecko_id']='';
	$unique[$one]['count_gecko']=0;
	$unique[$one]['count_paprika']=0;
}
foreach ($cccoins as $one) {unset($unique[$one]);}

foreach ($gecko as $one) {
	$symbol=strtoupper($one['symbol']);
	if (isset($unique[$symbol])){
		if (isset($genuine[$one['id']])||($unique[$symbol]['count_gecko']==0)){
			$unique[$symbol]['gecko_id']=$one['id'];
			$unique[$symbol]['count_gecko']=1;
		}
	}
}
foreach ($paprika as $one) {
	if (isset($unique[$one['symbol']])){
		$unique[$one['symbol']]['price_btc']=satoshi($one['price_btc']);
		$unique[$one['symbol']]['count_paprika']++;
	}
}
$id_list="";$sep="";
foreach ($unique as $one => $details){
	$unique[$one]['n_providers']=0;
	if ($details['count_gecko']==1) {
		$id_list.=$sep.$details['gecko_id'];
		$sep=",";
	}
	if ($details['count_paprika']==1) {$unique[$one]['n_providers']=1;}
}
if ($id_list!="") {
	/* coinpaprika price has been put in 'price_btc' above
	   Next we average it with the price of coingecko (There are only two providers; If there are more we need this extra counter: n_providers)
	   Maybe a warning when prices are too different
	   NB. If coinpaprika has non-unique symbols ('count_paprika'>1) we don't use it
	*/
	$url="https://api.coingecko.com/api/v3/simple/price?ids=$id_list&vs_currencies=BTC&include_market_cap=true&include_24hr_vol=true&include_24hr_change=true&include_last_updated_at=true&precision=18";
	$all=json_decode(file_get_contents($url),1);
	foreach ($all as $one=>$details){
		foreach ($unique as $coin=>$unique_details) {
			if ($unique[$coin]['gecko_id']==$one){
				$price=satoshi($details['btc']);
				if ($unique[$coin]['count_paprika']==1) {
					$price+=satoshi($unique[$coin]['price_btc']);
					$price=satoshi($price/2);
					$unique[$coin]['n_providers']=2;
				} else {
					$unique[$coin]['n_providers']=1;
				}
				$unique[$coin]['price_btc']=$price;
				break;
			}
		}
	}	
}
$cex="";$sep="";
foreach ($unique as $one => $details){
	if ($details['n_providers']>=1) {
		$cex.=$sep.$one.":".$details['price_btc'];
		$sep="\n";
	}
}
file_put_contents(ROOT."cex_prices.txt",$cex);

function pairs($coinx,$password){
	$data='{"coin":"'.$coinx.'","method":"best_orders","action":"buy","volume":"10","userpass":"'.$password.'"}';
	$cmd="curl --silent --url 'http://127.0.0.1:7783' --data '$data'";
	$result=shell_exec($cmd);
	if (strpos($result,'"error":')>0) {
		echo $result;
	} else {
		$a=json_decode($result,1);
		$b=$a["result"];
		foreach ($b as $coin => $c){
			$foreign=explode("-",$coin);
			if (count($foreign)==1) {$coins[$coin]="b";}
		}
	}
	$coins['BTC']="b";  
	$coins['LTC']="b";
	$coins['KMD']="b";
	$cccoins=explode(",",file_get_contents(ROOT."cccoins"));
	foreach ($cccoins as $coin) {$coins[$coin]="b";}
	// Only buys are interesting, because you can allways sell while there is no active market; CC, BTC, LTC and KMD are exceptions
	ksort($coins);
	$pairs="";$sep="";
	foreach ($coins as $coin =>$markt) {
		$pairs.="$sep$coin";
		$sep="\n";
	}
	return ($pairs);
}
function satoshi($x){ return substr(number_format((float)$x,8,".",""),0,10);}

?>
