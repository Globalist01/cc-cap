# The Market-merge subproject

Merge all market-orders in one buy and sell list for selected cryptocurrencies

All supported coins can be added to the file [./data/cccoins](https://gitlab.com/c4319/cc-cap/-/blob/main/Market-Merge/data/cccoins)

All CEX-markets can be added to the file [./data/markets](https://gitlab.com/c4319/cc-cap/-/blob/main/Market-Merge/data/markets)

Note that the atomicdex markets are discovered and accessed automatically

## API

Available data are 

- Merged markets: [marketmerge/?coin](https://communitycoins.org/marketmerge/?EFL)  (View page-source)
```
"$price\t$amount\t$sum\t$bitcoin\t$dollar\t$market\n";
```
The market is presented with four indicators
```
"market name"-"Base coin Tikker"-"Related coin Tikker"-"Buy or Sell"
```

- Latest prices: [marketmerge/?latest](https://communitycoins.org/marketmerge/?latest)  (View page-source)
```
coin:price\t...
```

- History of prices: [marketmerge/?history](https://communitycoins.org/marketmerge/?his)
```
coin:price\t...
```

- Latest market statistics: [marketmerge/?stat&coin](https://communitycoins.org/marketmerge/?stat)
```
{"COIN":{"coins_for_sale":,"price":"","highest_bid":"","lowest_ask":"","bitcoins_for_sale":""},{...}}
' coins_for_sale is the amount of coins locked because they are for sale
```

- History of market statistics: [marketmerge/?stat_history&coin](https://communitycoins.org/marketmerge/?stathis&EFL)
```
time \t coins_for_sale \t price \t highest_bid \t lowest_ask \t bitcoins_for_sale
...
...
```

- Exchange rates: [marketmerge/?rates](https://communitycoins.org/marketmerge/?rates)
```
{"rates":{"currency":{"name":"","unit":"","value":,"type":"fiat/crypto"},...
```
